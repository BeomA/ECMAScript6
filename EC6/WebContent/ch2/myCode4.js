/**
 * 
 *  let과 var, this
 *  
 *  this가 window 오브젝트를 참조하는데 window 오브젝트에 let변수가 없다는 것은 
 *  window 오브젝트에 let 변수가 설정되지 않았다는 의미가 된다. 
 *  이점이 var 변수과 let변수의 차이
 * 
 */
//"use strict";
//debugger;

var music = "음악";
console.log(this.music);

let sports = "축구";
console.log(this.sports);