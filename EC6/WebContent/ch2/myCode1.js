/**
 * use strict 작성 이유 : Class 오브젝트와 같은 일부 오브젝트는 strict 모드에서 실행이 기본이기 때문
 * with문과 같이 특별한 환경에서 strict 모드를 사용하지 못할 시 주석 처리
 * 
 * debugger 키워드는 별도의 조치를 취하지 않고 바로 소스 코드를 라인 단위로 디버깅할 수 있어 편리함
 * 
 *            var 키워드
 * 
 * var는 변수 언언시 사용, 로컬 변수와 글로벌 변수로 구분
 * 변수 구분 이유는 SCOPE 때문
 * 
 * 로컬변수는 함수 또는 오브젝트를 스코프로 사용하려는 의도, 글로벌 변수는 프로그램 전체에서 공용으로 사용하려는 의도
 * 
 */
//"use strict";
debugger;

//var 키워드 사용하지 않았으므로 one은 글로벌 변수
one=100;
function get(){
	
	one = 300;  //함수안에 one 변수가 없으니 함수 밖에서 검색
	console.log("함수:", one);
	
}

console.log("get() 전: ",one);
get();
console.log("글로벌:",one);

