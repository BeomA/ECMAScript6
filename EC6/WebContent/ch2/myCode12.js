/**
 * use strict 작성 이유 : Class 오브젝트와 같은 일부 오브젝트는 strict 모드에서 실행이 기본이기 때문
 * with문과 같이 특별한 환경에서 strict 모드를 사용하지 못할 시 주석 처리
 * 
 * debugger 키워드는 별도의 조치를 취하지 않고 바로 소스 코드를 라인 단위로 디버깅할 수 있어 편리함
 */
"use strict";
debugger;

const SPORT = "축구";
try{
	SPORT = "농구";
}catch (e){
	console.log("const 재할당 불가");
}