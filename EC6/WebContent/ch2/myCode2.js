/**
 * let sports = "축구";
 * 
 * let은 참고 범위?(스코프 범위)가 작다
 * 
 * 1. 함수안에 작성한 let 변수는 함수가 스코프
 * 2. 함수 안에 if(a = b) {let sports = "축구"} 형태로 코드를 작성했을 때, sports 변수는 함수가 스코프가 아니라
 * if 문의 블록 {}이 스코프이다
 * 3. 블록{} 밖에 같은 이름의 변수가 있어도 스코프가 다르므로 변수 각각에 값을 설정할 수 있으며 변수 값이 유지된다.
 * 4. 블록 안에 블록을 계층적으로 작성하면 각각의 블록이 스코프이다.
 * 5. 같은 스코프에서 같은 이름의 let 변수를 선언할 수 없다.
 * 6. let 변수는 호이스팅(hoisting)되지 않습니다. 
 * 
 * 
 * 
 */
"use strict";
debugger;

let book;
let sports= "축구";
sports = "농구";

//let sports="배구";
let one = 1, two=2, three;
//let four =4, let five=5;
//let six=6, var seven=7;
